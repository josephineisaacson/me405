
"""
Created on Wed Jan  6 08:50:21 2021
@file HW00x01.py
@author: Josephine Isaacson
This file contains a function which takes an input payment and price to return change in the least denominations
"""
#math is used to round down to the nearest integer using math.floor
import math

def GetChange(price, payment):
    '''
    @brief This function takes a price and given payment to calculate change using the lowest number of coins/bills
    @var price The price of the desired object in total cents
    @var payment The payment from the user as a tuple in the form : (pennies, nickels, dimes, quarters, ones, fives, tens, twenties) 
    '''
    
    #Take the input tuple and sum the components, scaling them to their dollar value
    #would've made sense to use cents instead but dollars makes more sense to me in calculating
    paymentvalue = payment[0]*.01 + payment[1]*.05 + payment[2]*.1 + payment[3]*.25 + payment[4]*1 + payment[5]*5 + payment[6]*10 + payment[7]*20
    
    #multiply by 100 to change from dollars to cents
    remainder = (paymentvalue*100) - price
    
    #Check if the user paid enough
    if remainder < 0:
        print('You have not paid the total cost')
    
        #Check if the user gave the exact amount
    elif remainder == 0:
        print('No change needed')
    
    #If the user needs change, calculate the tuple    
    elif remainder > 0:
        
        #by starting with 20's the least amount of denominations will be needed
        if remainder/2000 >= 1:
            #Define k as the number of twenties needed to return (rounded down)
            k = math.floor(remainder/2000)
            
        else:
            #This accounts for there not being a certain type of currency but checking for lower ones
            #k will be the last value in the change tuple
            k = 0
            
        #l is the new remainder (the original remainder minus the value of a 20 times the number of 20s
        #This will be used to repeat the process with each type of currency until the leftover value is converted into pennies
        
        l = remainder - 2000*k
            
        #10s
        
        if l/1000 >= 1:
            #The nummber of 10s needed without rounding
            m = math.floor(l/1000)
                
        else:
            m=0

        #new remainder
        #the process is the same the whole way through with different values
        n = l - 1000*m
                
        #5s
        if n/500 >= 1:
                    
            # The number of 5s
            o = math.floor(n/500)                   
                    
        else:
            o=0
            
            
        p = n - 500*o
                    
        if p/100>= 1:
                        
            #the number of 1s
            q = math.floor(p/100)
                        
        else:
            q=0
        
        r = p - 100*q
                        
        if r/25 >= 1:
                            
            #the number of quarters
            s = math.floor(r/25)
            
        else:
            s=0
                            
        t = r - 25*s
                            
        if t/10 >= 1:
                                
            #the number of dimes
            u = math.floor(t/10)
        else:
            u=0
                                
        v = t - 10*u
            
                              
        if v/5 >= 0:
                                    
            #the number of nickels
            w = math.floor(v/5)
            
        else:
            w = 0
                                    
        #The numberof pennies
        x = v - w*5
        
        #This tuple contains how many of each type of currency to give back
        #I changed x to an integer because it doesn't get rounded down, so it would always have a .0 otherwise
        change = (int(x), w, u, s, q, o, m, k)                             
    
        #show the user their change                            
        print(change)
        
