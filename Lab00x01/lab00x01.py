"""
Created on Tue Jan 12 12:13:59 2021

@author: josephine
"""

class VendingMachine:
    '''
    @brief A finite state machine defining the behavior of a soda vending machine
    '''
    ## Constant defining state 0 - initialization
    S0_INIT = 0
        
    ## Constant defining state 1 - Display
    S1_DISP = 1
        
    ## Constant defining state 2 - Change
    S2_CHAN = 2
        
    ## Constant defining state 3 - Eject
    S3_EJEC = 3
        
    ## Constant defining state 4 - Dispense
    S4_DISP = 4
    
    def __init__(self, dime, nickel, penny, quarter):
        '''
        Creates a Vending Machine task 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        
        
def GetChange():
    '''
    '''
    pass

def printWelcome():
    '''
    '''
    pass    
        
def        
        